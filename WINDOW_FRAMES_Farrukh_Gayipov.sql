/*
Common Table Expression (CTE): WeeklySales
This part of the query creates a Common Table Expression (CTE) named WeeklySales.
The 'DISTINCT ON' clause ensures that only the first occurrence of each unique combination of
(calendar_week_number, time_id, day_name) is considered, preventing duplicate rows.
The selected columns include calendar_week_number, time_id, day_name, amount_sold.
'cum_sum' represents the cumulative sum of amount_sold over time.
'centered_3_day_avg' is the rounded average of amount_sold over a window of three days (one preceding and one following) ordered by time,
with a partition by day_name to calculate the centered average based on the day of the week.
Main Query:
The main query selects columns from the WeeklySales CTE.
'amount_sold' is aliased as sales for clarity in the result set.
The final result includes the sales report for the 49th, 50th, and 51st weeks of 1999, with cumulative sum and centered 3-day average.
In summary, the modified query generates a sales report based on the specified conditions,
utilizing a CTE to calculate cumulative sum and a centered 3-day average for each day within the specified weeks,
with additional partitioning by day_name for accurate centered averages based on the day of the week.
*/

WITH WeeklySalesAdjusted AS (
    SELECT DISTINCT ON (t.week_number, s.time_id, t.day_name)
        t.week_number,
        s.time_id,
        t.day_name,
        s.amount_sold,
        SUM(s.amount_sold) OVER (ORDER BY t.time_id) AS total_sales,
        ROUND(AVG(s.amount_sold) OVER (
            PARTITION BY t.day_name
            ORDER BY t.time_id
            ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
        ), 2) AS avg_3_day_centered
    FROM
        sales_data s
        JOIN time_data t ON s.time_id = t.time_id
    WHERE
        t.calendar_year = 1999
        AND t.week_number IN (49, 50, 51)
)
SELECT
    week_number AS calendar_week_number,
    time_id,
    day_name,
    amount_sold AS sales,
    total_sales AS cum_sum,
    avg_3_day_centered AS centered_3_day_avg
FROM
    WeeklySalesAdjusted;